package com.example.firebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.SimpleAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.txAlamat
import kotlinx.android.synthetic.main.activity_main.txNama
import kotlinx.android.synthetic.main.activity_main.txTelp
import kotlinx.android.synthetic.main.row_data.*

class MainActivity : AppCompatActivity() , View.OnClickListener {
    val COLLECTION = "mahasiswa"
    val F_ID = "id"
    val F_NAMA = "nama"
    val F_ALAMAT = "alamat"
    val F_TELP = "telp"
    var docId = ""
    lateinit var db : FirebaseFirestore
    lateinit var alMhs : ArrayList<HashMap<String, Any>>
    lateinit var adapter : SimpleAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alMhs = ArrayList()
        btnTambah.setOnClickListener(this)
        btnEdit.setOnClickListener(this)
        btnHapus.setOnClickListener(this)
        lsMhs.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance()
        db.collection(COLLECTION).addSnapshotListener { querySnapshot, e ->
            if(e != null) Log.d("firestore",e.message)
            showData()
        }

    }
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnTambah -> {
                val hm = HashMap<String, Any>()
                hm.set(F_ID,txId.text.toString())
                hm.set(F_NAMA,txNama.text.toString())
                hm.set(F_ALAMAT,txAlamat.text.toString())
                hm.set(F_TELP,txTelp.text.toString())
                db.collection(COLLECTION).document(txId.text.toString()).set(hm).addOnSuccessListener {
                    Toast.makeText(this, "Data berhasil ditambah", Toast.LENGTH_SHORT).show()
                }.addOnFailureListener { e ->
                    Toast.makeText(this,"Data gagal ditambah : ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
            R.id.btnEdit -> {
                val hm = HashMap<String, Any>()
                hm.set(F_ID,docId)
                hm.set(F_NAMA,txNama.text.toString())
                hm.set(F_ALAMAT,txAlamat.text.toString())
                hm.set(F_TELP,txTelp.text.toString())
                db.collection(COLLECTION).document(docId).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this,"Data berhasil diedit", Toast.LENGTH_SHORT).show()
                    }.addOnFailureListener { e ->
                        Toast.makeText(this,"Data gagal diedit : ${e.message}", Toast.LENGTH_SHORT).show()
                    }
            }
            R.id.btnHapus -> {
                db.collection(COLLECTION).whereEqualTo(F_ID,docId).get().addOnSuccessListener {
                        results ->
                    for(doc in results){
                        db.collection(COLLECTION).document(doc.id).delete()
                            .addOnSuccessListener {
                                Toast.makeText(this, "Data berhasil dihapus", Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener { e ->
                                Toast.makeText(this, "data gagal dihapus : ${e.message}", Toast.LENGTH_SHORT).show()
                            }
                    }
                }.addOnFailureListener { e ->
                    Toast.makeText(this,"Tidak mendapatkan data ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alMhs.get(position)
        docId = hm.get(F_ID).toString()
        txId.setText(docId)
        txNama.setText(hm.get(F_NAMA).toString())
        txAlamat.setText(hm.get(F_ALAMAT).toString())
        txTelp.setText(hm.get(F_TELP).toString())
    }

    fun showData(){
        db.collection(COLLECTION).get().addOnSuccessListener { result ->
            alMhs.clear()
            for(doc in result){
                val hm = HashMap<String, Any>()
                hm.set(F_ID,doc.get(F_ID).toString())
                hm.set(F_NAMA,doc.get(F_NAMA).toString())
                hm.set(F_ALAMAT,doc.get(F_ALAMAT).toString())
                hm.set(F_TELP,doc.get(F_TELP).toString())
                alMhs.add(hm)
            }
            adapter = SimpleAdapter(this, alMhs,R.layout.row_data,
                    arrayOf(F_ID,F_NAMA,F_ALAMAT,F_TELP),
                    intArrayOf(R.id.txId,R.id.txNama, R.id.txAlamat, R.id.txTelp))
            lsMhs.adapter = adapter
        }
    }

}